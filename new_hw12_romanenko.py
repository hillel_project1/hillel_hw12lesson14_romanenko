import time

def timer_decorator(foo):
    """Decorator to calculate how much time a specific function is in process"""
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = foo(*args, **kwargs)
        required_time = time.time() - start_time
        print(f'Time required to perform this function is {required_time}')
        return result
    return wrapper

@timer_decorator
def dummy_function(a,b):
    return a*b

print(dummy_function(5,5))

